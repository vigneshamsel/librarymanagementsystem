package librarymanagement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Books {
    int enrollNumber,PublishedYear;
    String name,authorName,genre,publishers,ISBN;
    Boolean Status = true;
    LocalDate returnDate =   LocalDate.now();
    public Books(String name,String Isbn,int roll, int year,String authorname,String publisher,String genre){
    this.name = name;
    this.ISBN = Isbn;
    this.PublishedYear = year;
    this.enrollNumber = roll;
    this.authorName = authorname;
    this.publishers = publisher;
    this.genre = genre;
    this.returnDate=LocalDate.now();
    }
    
    public Books() {
    }
    @Override
    public String toString(){
        return String.format("\n"+"   "+enrollNumber+"\t"+"  "+name+"\t"+authorName+"\t"+genre+"\t"+publishers+"\t"+"    "+ISBN+"\t"+PublishedYear);
    
    }
     public ArrayList<Books> getInitialBooks(){
       ArrayList<Books> booklist = new ArrayList<>();
       booklist.add(new Books("Leaders eat least","6782877",1,1965,"Simon sinek","Nobell","Buisness"));
       booklist.add(new Books("Its your life live","7568489",2,1974,"Jos minds","Dolphin","Buisness"));
       booklist.add(new Books("Fault in our stars","6567886",3,1984,"john green","Nobell","entertainment"));
       booklist.add(new Books("jab jab right hook","7688789",4,1997,"Gary neychurk","lica","entertainment"));
       booklist.add(new Books("Thinking fast ","6959034",5,1995,"Robert kiyoski","Dolphin","technical"));
       booklist.add(new Books("Rich dad poor dad","5799994",6,1986,"Simon sinek","lica","Buisness"));
       booklist.add(new Books("the origin of species","5795345",7,1987,"Simon sinek","Nobell","technical"));
       booklist.add(new Books("julius caesar Assain","6799234",8,1997,"Robert kwang","parvit","storyBased"));
       booklist.add(new Books("the first  thing","1234654",9,1964,"Gary neychurk","parvit","storyBased"));
       return booklist;
    }
     public ArrayList<Books> returnBook(ArrayList<Books> list,int rollNumber,User foundUser){
           Books returnBook = null;
           int fineamount =0;
           System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
           for(Books its:list){
                 if(its.enrollNumber==rollNumber){
                   returnBook = its;
                 }
           }
           returnBook.Status = true;
           LocalDate today = LocalDate.now();
           int n =today.compareTo(returnBook.returnDate);
           if(n>0){
               fineamount = n*5;
           }
          foundUser.booksHaving--;
          System.out.println("Bookshaving"+foundUser.booksHaving);
          System.out.println("Returned succesfully"+"\n"+"Fine Amount"+fineamount);
          System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
                return list;
     }
    public ArrayList<Books> requestBook(ArrayList<Books> list,Books requestedBook,User user){
           if(requestedBook.Status){
           requestedBook.Status = false;
           LocalDate today = LocalDate.now();
           requestedBook.returnDate = today.plusDays(60);
           user.booksHaving++;
           System.out.println(user.name+"  "+"BooksHaving"+user.booksHaving);
           System.out.println("hired successfully"+"return date"+requestedBook.returnDate);
           System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
           return list;}
           else{
           System.out.println("Already hired \n EXpected return"+requestedBook.returnDate);
           System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
           return list;
           }
    }
     public ArrayList<Books> searchByName(ArrayList<Books> list,User user){
        Books requestedBook = null;
        boolean bookNotFound = true;
        Books obj = new Books();
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter words that the name of the book contains");
        String names = scan.nextLine();
        System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
        for(Books iterations:list)
        {
        if(iterations.name.toLowerCase().contains(names.toLowerCase())){
            System.out.println(iterations);
            bookNotFound=false;
        }}
        if(bookNotFound){System.out.print("Book Not found Try Again");
         int selection = scan.nextInt();
          switch(selection){
              case 1:
              list=obj.searchByName(list, user);
              case 2:
              System.exit(0);
          }
        System.exit(0);}
        list =obj.searchByRollno(list,user);
        return list;}
        
        
   
    public ArrayList<Books> searchByRollno(ArrayList<Books> list,User user){
        Books requestedBook = null;
        boolean bookNotFound = true;
        Scanner scan = new Scanner(System.in);
        Books obj = new Books();
        System.out.println("Enter the Rollnumber of book");
        int roll = scan.nextInt();
        System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
        for(Books iterations:list){
        if(iterations.enrollNumber==roll){
            System.out.println(iterations);
            bookNotFound =false;
            requestedBook=iterations;
        }}
        if(bookNotFound){
        System.out.print("Roll Number not matched \n1.Try Again \n2.Exit");
          int selection = scan.nextInt();
          switch(selection){
              case 1:
              list=obj.searchByRollno(list, user);
              case 2:
              System.exit(0);
          }
        
        }
        System.out.println("Enter your selection"+"\n"+"1.Request book"+"2.exit");
        int n = scan.nextInt();
        switch(n){
            case 1:
                list=obj.requestBook(list,requestedBook,user);
                break;
            case 2:
                System.exit(0);
                break;
        }
            return list;
       }
    public ArrayList<Books> searchByauthor(ArrayList<Books> list,User user){
        Books obj = new Books();
        Books bookrequested = null;
        boolean bookNotFound = true;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Author's name");
        String names = scan.nextLine();
        System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
        for(Books it:list){
        if(it.authorName.toLowerCase().contains(names)){
            System.out.println(it);
            bookNotFound=false;
        }}
        if(bookNotFound)
            {System.out.print("Book Not found Try Again");
         int selection = scan.nextInt();
          switch(selection){
              case 1:
              list=obj.searchByName(list,user);
              case 2:
              System.exit(0);
          }}
        return list;
    }

                
                
        public ArrayList<Books> searchByPublishingYear(ArrayList<Books> list,User user){
        Books requestBook = null;
        boolean BookNotFound = true;
        Books obj = new Books();
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Publishingyear");
        int year = scan.nextInt();
        System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
        for(Books it:list){
        if(it.PublishedYear== year){
            System.out.println(it);
            BookNotFound=false;
        }}
        if(BookNotFound){
          System.out.print("Book Not found");
         int selection = scan.nextInt();
          switch(selection){
              case 1:
              list=obj.searchByName(list,user);
              case 2:
              System.exit(0);
          }}
        list =obj.searchByRollno(list,user);
        return list;
       }
    public ArrayList<Books> searchByPublication(ArrayList<Books> list,User user){
        Books req = list.get(0);
        boolean value = true;
        Books obj = new Books();
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Pulications");
        String publisher = scan.nextLine();
        System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
        for(Books it:list){
        if(it.publishers.equalsIgnoreCase(publisher)){
            System.out.println(it);
            value=false;
        }}
        if(value){System.out.print("not found");
         int selection = scan.nextInt();
          switch(selection){
              case 1:
              list=obj.searchByRollno(list, user);
              case 2:
              System.exit(0);
          }}
        list =obj.searchByRollno(list,user);
        return list;
       }
    public ArrayList<Books> searchByGenre(ArrayList<Books> list,User user){
        Books requestBook = null;
        boolean bookNotFound = true;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the genre");
        System.out.println("Genre available: story,Buisness,technical,entertainment");
        String genr = scan.nextLine();
        System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
        for(Books it:list){
        if(it.genre.equals(genr)){
            System.out.println(it);
            bookNotFound=false;
        }}
        if(bookNotFound){System.out.print("Book not found  Try Again");
        System.exit(0);}
        Books obj = new Books();
        list =obj.searchByRollno(list,user);
        return list;
       }
    public ArrayList<Books> addBook(ArrayList<Books> list){
    ArrayList<Books> arraylist = list;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the book name");
        String name = scan.nextLine();
        System.out.println("Enter the ISBN");
        String ISBN = scan.nextLine();
        System.out.println("Enter the roll number");
        int rollNo = scan.nextInt();
        System.out.println("Enter the Publishing Year");
        int year = scan.nextInt();
        System.out.println("Enter the author name");
        scan.nextLine();
        String authorname = scan.nextLine();
        System.out.println("Enter the publisher");
        String publisher = scan.nextLine();
        System.out.println("Enter the genre");
        String genre = scan.nextLine();
        arraylist.add(new Books(name,ISBN,rollNo,year,authorname,publisher,genre)); 
        System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
        return arraylist;
    }
    public ArrayList<Books> removeBook(ArrayList<Books> list){
        Scanner scan = new Scanner(System.in);
        ListIterator<Books> it;
        it = list.listIterator();
        System.out.println("search by");
        System.out.println("1.book name \n"+"2.ISBN"+"\n"+"3.rollnumber"+"\n4.exit");
        int num = scan.nextInt();
        scan.nextLine();
        switch(num){
            case 1:
            System.out.println("Enter the Bookname");
            String nam = scan.nextLine();
            while(it.hasNext()){
                if((it.next().name.contains(nam))||(it.next().name.equals(nam))){
                    list.remove(it.previous());
                    break;
                }
            }
            System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
            break;
            case 2:
            System.out.println("Enter the ISBN");
            String isb = scan.nextLine();
            while(it.hasNext()){
                if((it.next().ISBN.equals(isb))){
                    list.remove(it.previous());
                    break;
                }
            }
            System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
            break;
            case 3:
            System.out.println("Enter the Rollnumber");
            int roll= scan.nextInt();
            while(it.hasNext()){
                if((it.next().enrollNumber==roll)){
                  list.remove(it.previous());
                    System.out.println("no");
                    break;
                }}
            System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
                    break;
        }
        
        System.out.println("Enter your choice \n 1.Return to mainmenu 2.Exit");
        return list;
    }}