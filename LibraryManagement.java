package librarymanagement;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public class LibraryManagement {
 public static void main(String[] args) {
  Scanner    scanner    = new Scanner(System.in);
  Books      book       = new Books();
  Librariyan librariyan = new Librariyan();
  User       user       = new User();
  ArrayList < Books > books  = book.getInitialBooks();
  HashMap <String,String> librariyans = librariyan.getLibrariyans();
  LinkedList<User>  users = user.getUsers();
  HashMap<String,String> userCheck = new HashMap<>();
        for(User its: users){
             userCheck.put(its.name,its.password);
        }
  System.out.println("ROLLNUMBER\tBOOKNAME\tAUTHORNAME\tGENRE   \tPUBLICATIONS  ISBN\tPUBLISHING YEAR");
  System.out.println(books);
  mainmenu: while(true){
  System.out.println("Enter your selection" + "\n" + "1.librariyan" + "\n" + "2.Customer"+"\n" + "3.new user  \n4.New libraiyan");
  int selection= scanner.nextInt();
  switch (selection) {
   case 1:
    librariyan.verifylibrarian(librariyans);
    System.out.println("enter your selection" + "\n" + "1.AddBooks" + "\n" + "2.RemoveBooks" + "\n3.SummaryReport"+"4.exit");
    int numb = scanner.nextInt();
    switch (numb) {
     case 1:
      books = book.addBook(books);
      if(scanner.nextInt()==1){continue mainmenu;}
      break;
     case 2:
      books = book.removeBook(books);
      if(scanner.nextInt()==1){continue mainmenu;}
      break;
     case 3:
      System.out.println("enter your selection \n 1.availability report 2.report by Genre \n 3.report by Author \n 4.report by publisher ");
      int num = scanner.nextInt();
      switch (num) {
       case 1:
        librariyan.printTotalNumberOfBooks(books);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 2:
        librariyan.printbyGenre(books);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 3:
        librariyan.printbyAuthor(books);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 4:
        librariyan.printbyPublisher(books);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
      }
      break;
    }
   case 2:
    String username= user.verifyuser(userCheck);
    User foundUser=null;
    for(User its:users)
    {
        if (its.name.equalsIgnoreCase(username))
        {foundUser=its;}
    }
    System.out.println("Enter your selection\n  1.request 2.return");
    int no = scanner.nextInt();
    switch (no) {
     case 1:
      if (foundUser.booksHaving > 1) {
       System.out.println("Already hired");
       System.exit(0);
      } else
       System.out.println("Enter your selection" + "\n" + "1.search by name" + "\n" + "2.search by rollnumber" + "\n" + "3.search by author name" + "\n" + "4.search by publisher" + "\n" + "5.search by year of publication" + "\n" + "6.search by genre");
      int numbr = scanner.nextInt();
      switch (numbr) {
       case 1:
        books = book.searchByName(books,foundUser);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 2:
        books= book.searchByRollno(books,foundUser);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 3:
        books = book.searchByauthor(books,foundUser);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 4:
        books = book.searchByPublication(books,foundUser);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 5:
        books = book.searchByPublishingYear(books,foundUser);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
       case 6:
        books = book.searchByGenre(books,foundUser);
        if(scanner.nextInt()==1){continue mainmenu;}
        break;
      }
      break;
     case 2:
      System.out.println("Enter the roll number of book");
      int rollno = scanner.nextInt();
      books = book.returnBook(books,rollno,foundUser);
      if(scanner.nextInt()==1){continue mainmenu;}
      break;
    }
   case 3:
       User newUser;
       newUser = user.addUser(users);
       userCheck.put(newUser.name, newUser.password);
       if(scanner.nextInt()==1){continue mainmenu;}
       break;
   case 4:
       librariyans = librariyan.Librariyan(librariyans);
       if(scanner.nextInt()==1){continue mainmenu;}
       break;
    }
  }
 }
}